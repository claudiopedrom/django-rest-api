# The first instruction is what image we want to base our container on
# We Use an official Python runtime as a parent image
FROM python:3.7.1

# The enviroment variable ensures that the python output is set straight
# to the terminal with out buffering it first
ENV PYTHONUNBUFFERED 1

# create root directory for our project in the container
RUN mkdir /rest_api

# Set the working directory to /rest_api
WORKDIR /rest_api

# Copy the requirements.txt into the container at /rest_api
ADD requirements.txt /rest_api/

# Install any needed packages specified in requirements.txt
RUN pip install -r requirements.txt

# Copy the current directory contents into the container at /rest_api
ADD . /rest_api/